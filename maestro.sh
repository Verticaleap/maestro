#!/bin/sh

# This script is used to baseline config files against a default repository
# The repository should be a git repo, specified with the git_repo variable
# Configs for server are stored within the git repo using the following structure:
#
# This is a very rough draft at this time, and should not be used anywhere important
#
# This script requires the following environmental variables to be set for gitlab access:
#
# MAESTRO_GIT_SERVER
# MAESTRO_GIT_REPO
# MAESTRO_GIT_USER
# MAESTRO_GIT_TOKEN

#################
### VARIABLES ###
#################

# Get script name
scriptname=$(basename "$0")

required_vars="MAESTRO_GIT_SERVER MAESTRO_GIT_REPO MAESTRO_GIT_USER MAESTRO_GIT_TOKEN"
precheck_fail="false"

# Primary maestro repo for plugins
maestro_repo="https://gitlab.com/Verticaleap/maestro.git"

maestro_working_dir="/tmp/maestro"
mkdir $maestro_working_dir 2>/dev/null

# Create a temp directory for files
tmpdir=$(mktemp -d -p $maestro_working_dir)

# Create folder for main maestro repo
maestro="$maestro_working_dir/repo"

# List of plugins loaded
loaded_plugins=$tmpdir/loaded_plugins

repo_hosts="$tmpdir/hosts"
repo_groups="$tmpdir/groups"
repo_files="$tmpdir/files"

# Colors for printf
red='\033[1;91m'
grn='\033[1;92m'
ylw='\033[1;93m'
end='\033[0m'

#################
### FUNCTIONS ###
#################

usage() {
    echo "
$scriptname [-u]
    
    -h      Show this usage

    -u      Update maestro

    -v      Use verbosity
"
    clean_up 2
}

log() {
    message="$1"
    echo "$message"
    logger -t $scriptname "$message" 
}

msg() {

    color="$1"
    message="$2"

    if [ "$verbose" = "true" ]
    then
        if [ "$color" = "grn" ]
        then
            printf "${grn}$message${end}\n"
        elif [ "$color" = "ylw" ]
        then
            printf "${ylw}$message${end}\n"
        elif [ "$color" = "red" ]
        then
            printf "${red}$message${end}\n"
        else
            printf "$message\n"
        fi
    fi
}

clean_up() {

    # Clean up all directories created if script exits or is terminated

    # If no exit code is specified, exit 0
    if [ -z $1 ]
    then
        exit_code=0
    else
        exit_code=$1
    fi

    # Remove the temporary directory
    if [ -d $tmpdir ]
    then
        rm -rf $tmpdir
    fi

    # Exit with proper exit code
    exit $exit_code
}

load_plugin() {

    plugin="$1"

    if [ -f "${maestro}/plugins/${plugin}.sh" ]
    then
        . ${maestro}/plugins/${plugin}.sh
    else
        echo "Plugin $plugin not found"
    fi
}

read_group_file() {

    file="$1"
    custom_var_block="$2"

    # Get list of id's in group file
    local ids_in_group=$(yaml_get_ids $file)
    
    # Loop over id's in file and call each plugin as necessary
    echo "$ids_in_group" |  while read -r name
    do
        
        # Get primary module in id
        local id_name=$(yaml_get_id_name "$name")        
        local id_module=$(yaml_get_id_module "$name" "$file")
        local id_variables=$(yaml_get_id_variable_block "$name" "$file")

        echo "task: [$id_name]"
        printf "$id_module: "

        # Load appropriate plugin
        if [ -n "$id_module" ]
        then
            load_plugin "$id_module"

            # Run plugin, and append any custom vars
            $plugin "$id_variables"
        else
            msg . "Could not find plugin: $plugin"
        fi

        # Put a space after task
        echo
    done
}

install_maestro() {
    # This can be used until a cron plugin is written, which should handle crontab location
    if [ "$(uname -s)" = "OpenBSD" ]
    then
        crontab="/var/cron/tab/root"
    else
        crontab="/etc/crontab"
    fi

    # Run install_cronjob group and append custom variables
    if [ -f $maestro/groups/install_cronjob ]
    then
        read_group_file $maestro/groups/install_cronjob
    fi
    clean_up
}

#############
### SETUP ###
#############

for var in $required_vars
do
    if [ -z "$(eval echo \$$var)" ]
    then
        echo "Missing $var"
        precheck_fail="true"
    fi
done

if [ "$precheck_fail" = "true" ]
then
    exit 1
fi

###############
### GETOPTS ###
###############

while getopts "hiuv" opt; do
    case $opt in
        h)
            usage
            ;;
        u)
            update=true
            ;;
        v)
            verbose=true
            ;;
        i)
            install=true
            ;;
        *)
            echo "invalid option: -$OPTARG"
            usage
            ;;
    esac
done


#############
### SETUP ###
#############

if [ -d $maestro ]
then
    msg . "Syncing main repo"
    git --git-dir=$maestro/.git pull -q origin master 2>/dev/null
    if [ "$?" -gt "0" ]
    then
        echo "error syncing repo, retrying"
        rm -rf $maestro
        git clone -q $maestro_repo $maestro 2>/dev/null
        if [ "$?" -gt "0" ]
        then
            echo "retry failed"
            clean_up 1
        fi
    fi
else
    msg . "Cloning maestro repo"
    git clone -q $maestro_repo $maestro
fi

# Load the yaml plugin, as it is required
. $maestro/plugins/yaml.sh

if [ "$install" = "true" ]
then
    if [ "$(id -u)" != "0" ]
    then
        echo "Install must be ran as root"
        clean_up 1
    fi
    install_maestro
fi


# Clone the repo containing the configs
git clone -q https://${MAESTRO_GIT_USER}:${MAESTRO_GIT_TOKEN}@${MAESTRO_GIT_SERVER}/${MAESTRO_GIT_REPO} $tmpdir
if [ "$?" != 0 ]
then
    echo "Clone of https://${MAESTRO_GIT_SERVER}/${MAESTRO_GIT_REPO}"
    clean_up 1
fi 

############
### MAIN ###
############

# Get servername, this could eventually become a type of fact
servername=$(hostname -s)

if [ "$verbose" = "true" ]
then
    echo "Servername: $servername"
fi

# Check if servername is in hosts folder
if [ -f "${repo_hosts}/$servername" ]
then
    # read group file to determine hosts' var block
    group_var_block=$(cat ${repo_hosts}/$servername)
    
    # Get group from group_var_block
    groups=$(echo "$group_var_block" | grep ^groups: | awk -F: '{print $2}')

    msg . "host groups: $groups"
else
    echo "$servername not found in $MAESTRO_GIT_SERVER/$MAESTRO_GIT_REPO"
    clean_up
fi

# Go through each group and list the files to be pulled into server
for group in $(echo $groups | tr ',' ' ')
do
    if [ -f "${repo_groups}/${group}" ]
    then
        read_group_file ${repo_groups}/$group
    else
        echo "Could not find $group"
    fi
done

clean_up
