#!/bin/sh

# This script includes the basic functionality to copy files around within a host

plugin="copy"
changed=0
failed=0

copy() {
    local variable_block="$1"
    local required_vars="src dest user group mode"

    # Ensure that required variables are all there
    for req in $required_vars
    do
        if [ -z "$(echo "$variable_block" | grep -c ^$req::)" ]
        then
            echo "Error: $plugin_name requires $req"
            fail=true
        fi
    done

    src=$(echo "$variable_block" | grep ^src:: | awk -F:: '{print $2}')
    dest=$(echo "$variable_block" | grep ^dest:: | awk -F:: '{print $2}')
    user=$(echo "$variable_block" | grep ^user:: | awk -F:: '{print $2}')
    group=$(echo "$variable_block" | grep ^group:: | awk -F:: '{print $2}')
    mode=$(echo "$variable_block" | grep ^mode:: | awk -F:: '{print $2}')
    
    # As long as the required vars are there, continue
    if [ "$fail" != "true" ]
    then
        msg . "\n  file: $dest"
        copy_file "$src" "$dest" 
        copy_user_group "$dest" "$user" "$group"
        copy_mode "$dest" "$mode"
    fi

    # Final messages, perhaps these should eventually be passed back to maestro?
    if [ "$verbose" != "true" ]
    then
        if [ "$failed" -gt "0" ] 
        then
            printf "${red}failed${end}\n"
        elif [ "$changed " -gt "0" ] 
        then
            printf "${ylw}changed${end}\n"
        else
            printf "${grn}ok${end}\n"
        fi
    fi
}

copy_file() {
    src="${repo_files}/$1"
    dest="$2"

    if [ -f "$src" ]
    then
        if [ "$(md5 -q $src 2>/dev/null | awk '{print $NF}')" != "$(md5 -q $dest 2>/dev/null | awk '{print $NF}')" ]
        then
            cp -f $src $dest 2>/dev/null
            rc=$?
            if [ "$rc" = "0" ]
            then
                msg ylw "  contents: changed"
                changed=$(( changed + 1 ))
            else
                msg red "  contents: failed"
                failed=$(( failed + 1 ))
            fi
        else
            msg grn "  contents: ok"
        fi
    else
        msg red "  contents: error"
        failed=$(( failed + 1 ))
    fi
}

copy_user_group() {
    dest="$1"
    user="$2"
    group="$3"
    fail=

    # Check to make sure user exists 
    if [ "$(getent passwd "$user" >/dev/null;echo $?)" -gt 0 ]
    then
        msg red "  user-group: error, user $user does not exist"
        failed=$(( failed + 1 ))
    fi

    # Check to make sure group exists in /etc/group
    if [ "$(getent group \"$group\" >/dev/null;echo $?)" -gt 0 ]
    then
        msg red "  user-group: error, group $group does not exist"
        failed=$(( failed + 1 ))
    fi

    if [ "$failed" -eq "0" ]
    then
        if [ -f "$dest" ]
        then
            if [ "${user}:${group}" != "$(stat $dest | awk '{print $5":"$6}')" ]
            then
                chown ${user}:${group} $dest 2>/dev/null
                rc=$?
                if [ "$rc" = "0" ]
                then
                    msg grn "  user-group: changed"
                    changed=$(( changed + 1 ))
                else
                    msg red "  user-group: fail"
                    failed=$(( failed + 1 ))
                fi
            else
                msg grn "  user-group: ok"
            fi
        else
            msg red "  user-group: error, $dest does not exist"
            failed=$(( failed + 1 ))
        fi
    fi
}

copy_mode() {
    dest="$1"
    mode="$2"

    if [ -f "$dest" ]
    then
        if [ "$mode" != "$(stat -f '%p' "$dest" | tail -c 5)" ]
        then
            chmod $mode $dest 2>/dev/null
            rc=$?
            if [ "$rc" = "0" ]
            then
                msg ylw "  mode: changed"
                changed=$(( changed + 1 ))
            else
                msg red "  mode: fail"
                failed=$(( failed + 1 ))
            fi
        else
            msg grn "  mode: ok"
        fi
    else
        msg red "  mode: error, $dest does not exist"
        failed=$(( failed + 1 ))
    fi
}

