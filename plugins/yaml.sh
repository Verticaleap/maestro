#!/bin/sh

# This is the plugin that contains all the yaml parsing functionality

# Example usage of plugin for testing:
#
# names=$(yaml_get_ids $file)
# 
# while read name
# do
#     id_module=$(yaml_get_id_module "$name")
#     id_name=$(yaml_get_id_name "$name")
#     id_content=$(yaml_get_id_content "$name" $file)
#     id_variables=$(yaml_get_id_variables "$name")
#     echo "id name: $id_name"
#     echo "id module: $id_module"
#     echo id_variables: 
#     echo "$id_variables"
#     eval $id_variables    
# 
#     echo $src
#     echo $dest
#     echo $content
# 
# done <<< "$names"


yaml_get_ids() {

    local file="$1"
    local id_count="1"
    
    file_names=$(grep '^.*- name:' $file)
    
    echo "$file_names" | while read -r line
    do
        # This is where each id should be filtered!


        # End of filtering
       
        # set id of each name 
        eval id${id_count}='$line'

        # echo out value
        eval echo \$id${id_count}

        # Increment up one value
        id_count=$((id_count + 1))

    done 
}

yaml_get_id_content() {

    local line="$1"
    local file="$2"

    # Escape existing forward slashes 
    line_filtered=$(echo $line | sed 's:/:\\/:g')

    # Get content of id
    id_content=$(sed -n -e "/$line_filtered/,/^$/ p" $file | grep -v '^#')

    echo "$id_content"
    
}

yaml_get_id_name() {

    local line="$1"

    # Get first uncommented line after name
    local id_name=$(echo "$line" | awk -Fname: '{print $NF}')

    echo $id_name
}

yaml_get_id_module() {

    local line="$1"
    local file="$2"

    # get content of id
    local id_content=$(yaml_get_id_content "$line" "$file")

    # Get first uncommented line after name
    local id_module=$(echo "$id_content" | grep -v '^#' | sed '1d' | head -1 | cut -d: -f1)

    # This errors out if there are double quotes around module name
    # Most likely this is due to any spaces present
    echo $id_module
}

yaml_get_id_variable_block() {
    
    local line="$1"
    local file="$2"

    local id_content=$(yaml_get_id_content "$line" "$file")
    local id_module=$(yaml_get_id_module "$name" "$file")

    local id_variables=$(echo "$id_content" | grep -v -e $id_module -e '- name:')

    echo "$id_variables" | while read var
    do
        if [ "$(echo "$var" | grep -c ':')" = "1" ]
        then
            key=$(echo "$var" | awk -F': ' '{print $1}')
            value=$(echo "$var" | awk -F': ' '{print $2}')
            echo "$key::$value"
        else
            echo "Error: $var is incorrectly formatted: missing colon"
        fi
    done
}
