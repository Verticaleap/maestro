#!/bin/sh

# This script includes the basic functionality to append text to a file

plugin="append"
changed=0
failed=0

append() {
    local variable_block="$1"
    local required_vars="dest content"

    # Ensure that required variables are all there
    for req in $required_vars
    do
        if [ "$(echo "$variable_block" | grep -c ^$req::)" = 0 ]
        then
            echo "error, $plugin requires $req"
            fail=true
        fi
    done

    dest=$(echo "$variable_block" | grep ^dest:: | awk -F:: '{print $2}')
    content=$(echo "$variable_block" | grep ^content:: | awk -F:: '{print $2}')
    create=$(echo "$variable_block" | grep ^create:: | awk -F:: '{print $2}')

    # As long as the required vars are there, continue
    if [ "$fail" != "true" ]
    then
        msg . "\n  file: $dest"
        create_file "$dest" "$create"
        append_line "$content" "$dest" 
    fi

    # Final messages, perhaps these should eventually be passed back to maestro?
    if [ "$verbose" != "true" ]
    then
        if [ "$failed" -gt "0" ]
        then
            printf "${red}failed${end}\n"
        elif [ "$changed " -gt "0" ]
        then
            printf "${ylw}changed${end}\n"
        else
            printf "${grn}ok${end}\n"
        fi
    fi
}

create_file() {

    dest="$1"
    create="$2"

    if [ ! -f "$dest" ]
    then
        if [ "$create" = "true" ]
        then
            touch $dest 2>/dev/null
            if [ "$?" = "0" ]
            then
                msg ylw "  create: changed"
                changed=$(( changed + 1 ))
            else
                msg red "  create: failed"
                failed=$(( failed + 1 ))
            fi
        fi
    else
        msg grn "  create: ok"
    fi
}

append_line() {

    content="$1"
    dest="$2"
    
    escaped_content=$(echo "$content" | awk '{gsub( /[(*|`$)]/, "\\\\&"); print $0}')

    if [ -f "$dest" ]
    then
        if [ "$(grep -c "$escaped_content" $dest)" -eq "0" ]
        then
            echo "$content" >> $dest
            rc=$?
            if [ "$rc" = "0" ]
            then
                msg ylw "  contents: changed"
                changed=$(( changed + 1 ))
            else
                msg red "  contents: failed"
                failed=$(( failed + 1 ))
            fi
        else
            msg grn "  contents: ok"
        fi
    else
        msg red "  contents: error, $dest does not exist. Create with 'create: true'"
        failed=$(( failed + 1 ))
    fi
}
